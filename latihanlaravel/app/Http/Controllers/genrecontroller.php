<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\genre;
use RealRashid\SweetAlert\Facades\Alert;

class genrecontroller extends Controller
{
    public function create(){
        return view('genre.create2');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama'],
        ]);

        Alert::success('Berhasil', 'Tambah Data Genre Berhasil');

        return redirect('/genre');
        
    }

    public function index(){
        $genre = genre::all();
        return view('genre.index', compact('genre'));
    }

    public function show($id){
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    public function edit($id){
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            
        ]);
        $query = DB::table('genre')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  
              ]);

              Alert::success('Update', 'Genre Berhasil Di Update');

              return redirect('/genre');
    }

    public function destroy($id){
        DB::table('genre')->where('id', $id)->delete();

        Alert::success('Delete', 'Genre Berhasil Dihapus');

        return redirect('/genre');

    }
}
