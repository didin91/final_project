<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\kritik;

class kritikcontroller extends Controller
{
    public function store(Request $request){
        $request->validate ([
            'isi' => 'required'
            ]);

            $kritik = new kritik;


        $kritik->isi = $request->isi;
        $kritik->user_id = Auth::id();
        $kritik->film_id = $request->film_id;
        
        $kritik->save();

        return redirect()->back();
    }
}
