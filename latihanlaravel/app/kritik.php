<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table='kritik';
    
    protected $fillable=['film_id','user_id', 'isi'];

    public function film()
    {
        return $this->belongsTo('App\film');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
