@extends('layout.master')

@section('Judul')
    HALAMAN TAMBAH FILM
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush

@section('content')

<form action="/film" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text"  name="judul" class="form-control" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('ringkasan')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror


    <div class="form-group">
        <label >Tahun</label>
      <input type="integer"  name="tahun" class="form-control" >
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Poster</label>
    <input type="file" name="poster" class="form-control" >
  </div>
  @error('poster')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror


<div class="form-group">
    <label>Genre Film</label> <br>
    <select name="genre_id" class="js-example-basic-single" style="width: 100%" id="">
    <option value="">---Pilih Genre---</option>
    @foreach ($genre as $item)
    <option value="{{$item->id}}">{{$item->nama}} </option>    
    @endforeach
    </select>
  </div>
  @error('genre_id')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



@endsection