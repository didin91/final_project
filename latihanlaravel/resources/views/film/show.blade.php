@extends('layout.master')

@section('Judul')
    Halaman Ringkasan {{$film->Judul}}
@endsection

@section('content')

<img src="{{asset('gambar/'. $film->Poster)}} " alt="">
<h1>{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>

<h1>Kritik</h1>

@foreach ($film->kritik as $item)
    <div class="card">
      <div class="card-body">
        <small><b>{{$item->user->name}}</b></small>
        <p class="card-text">{{$item->isi}} </p>
      </div>
    </div>
    @endforeach

<form action="/kritik" method="post" enctype="multipart/form-data" class="my-3">
    @csrf
    

<div class="form-group">
    <label>Berikan Kritik</label>
    <input type="hidden" name="film_id" value="{{$film->id}}" id="">
    <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('isi')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

<a href="/film" class='btn btn-secondary'>Kembali</a>


@endsection