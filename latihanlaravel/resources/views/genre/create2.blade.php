@extends('layout.master')

@section('Judul')
    HALAMAN FORM GENRE
@endsection

@section('content')

<form action="/genre" method="post">
    @csrf
    <div class="form-group">
      <label>Genre</label>
      <input type="text" name="nama" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection