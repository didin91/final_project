@extends('layout.master')

@section('Judul')
    HALAMAN LIST GENRE
@endsection

@section('content')

<a href="/genre/create2" class="btn btn-success mb-3">Tambah Data</a>

<table class="table table-dark" >
    <thead >
      <tr>
        <th scope="col">#</th>
        <th scope="col">Genre</th>
        <th scope="col">List Film</th>
        <th scope="col">Action</th>        
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)

        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$item->nama}} </td>
            <td>
              <ul>

                @foreach ($item->film as $value)
                  <li>{{$value->judul}}</li>
                @endforeach

              </ul>
             
            </td>
            <td>
                <form action="/genre/{{$item->id}}" method="POST">
                  <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm "  >Detail</a>
                <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  @method('delete')
                  @csrf
                  <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>    
            </td>
            </form>
            
          </td>
      </tr>

      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
      @endforelse
    </tbody>
  </table>  

  @endsection