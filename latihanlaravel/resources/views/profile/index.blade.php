@extends('layout.master')

@section('Judul')
    HALAMAN UPDATE PROFILE
@endsection

@push('script')

<script src="https://cdn.tiny.cloud/1/n3sj74565j5z5kbbibiz5zpp74b53fbxd2apbde2fi1hpgax/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script> 
@endpush

@section('content')


<form action="/profile/{{$profile->id}} " method="post">
    @csrf
    @method('put')

    <div class="form-group">
        <div class="form-group">
            <label>Nama User</label>
            <input type="text"   value="{{$profile->user->name}}" class="form-control" disabled>
          </div>
          <div class="form-group">
            <label>Email User</label>
            <input type="text"   value="{{$profile->user->email}}" class="form-control" disabled>
          </div>

        <div class="form-group">
      <label>Umur Profile</label>
      <input type="number"  name="umur" value="{{$profile->umur}}" class="form-control" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio"  class="form-control" >{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat"  class="form-control" >{{$profile->alamat}}</textarea>
  </div>
  @error('alamat')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection