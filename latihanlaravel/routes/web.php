<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('tugaslaravel.home');
})->middleware('auth');

Route::get('/register', function () {
    return view('tugaslaravel.register');
})->middleware('auth');

Route::get('/welcome', function () {
    return view('tugaslaravel.welcome2');
})->middleware('auth');

Route::get('/data-table', function () {
    return view('table.data-table');

})->middleware('auth');

Route::get('/table', function () {
    return view('table.table');

})->middleware('auth');

// CRUD cast
Route ::get('/cast/create', 'castcontroller@create')->middleware('auth');
Route ::post('cast/', 'castcontroller@store')->middleware('auth');
Route ::get('/cast', 'castcontroller@index')->middleware('auth');
Route ::get('/cast/{cast_id}', 'castcontroller@show')->middleware('auth');
Route ::get('/cast/{cast_id}/edit', 'castcontroller@edit')->middleware('auth');
Route ::put('/cast/{cast_id}', 'castcontroller@update')->middleware('auth');
Route ::delete('/cast/{cast_id}', 'castcontroller@destroy')->middleware('auth');

Route::group(['middleware' => ['auth']], function () {
    // CRUD genre
Route::get('/genre/create2', 'genrecontroller@create');
Route ::post('/genre', 'genrecontroller@store');
Route ::get('/genre', 'genrecontroller@index');
Route ::get('/genre/{genre_id}', 'genrecontroller@show');
Route ::get('/genre/{genre_id}/edit', 'genrecontroller@edit');
Route ::put('/genre/{genre_id}', 'genrecontroller@update');
Route ::delete('/genre/{genre_id}', 'genrecontroller@destroy');

// update profile
Route::resource('profile', 'profilecontroller')->only([
    'index', 'update'
])->middleware('auth');

Route::resource('kritik', 'kritikcontroller')->only([
    'index', 'store'
])->middleware('auth');
});



// CRUD film
Route::resource('film', 'filmcontroller')->middleware('auth');



Auth::routes();

 
